Simply save the script in your home directory (as `set-alias.sh` or whatever else you want), and add the following to your `.bashrc`:

```sh
source ~/set-alias.sh
```

Don't forget to use the right filename if you've changed it!

Now you can just use the `curlio` command:

```
sven@linux-etoq:~/archive/maagdenhuis_videos> curlio maagdenhuis4.flv 
######################################################################## 100.0%
http://curl.io/get/grlpigul/b0237b039e8e655a5b8c4723282aa9ae56c0e6a1
```

__Note:__ You'll need to have `jq` installed for this to work! It's probably shipped by your distribution.