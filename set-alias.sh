function curlio {
	curl --progress-bar -F "file=@$1" "https://curl.io/send/$(curl -s "https://curl.io/socket.io/?EIO=3&transport=polling&t=$(date +%s)-1&sid=$(curl -s "https://curl.io/socket.io/?EIO=3&transport=polling&t=$(date +%s)-0" | grep -oEa "\{.+" | jq -r .sid)" | grep -oEa "\[.+" | jq -r '.[1]')" | grep -oE https://curl.io/get/.+
}
export -f curlio
